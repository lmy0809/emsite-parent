package com.empire.emsite.modules.testwx.vo;

import java.util.Date;

/**
 * 类TestWxDateVO.java的实现描述：TODO 类实现描述
 * 
 * @author arron 2018年2月24日 下午6:27:06
 */
public class TestWxDateVO {
    private String id;
    private String openId;    // open_id
    private String name;      // 昵称
    private String remarks;   //备注
    private Date   updateDate; //更新时间

    public TestWxDateVO() {
        super();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
