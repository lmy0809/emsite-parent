/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.gen.facade;

import java.util.List;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.gen.entity.GenConfig;
import com.empire.emsite.modules.gen.entity.GenTable;

/**
 * 类GenTableFacadeService.java的实现描述：业务表FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:48:18
 */
public interface GenTableFacadeService {

    public GenTable get(String id);

    public Page<GenTable> find(Page<GenTable> page, GenTable genTable);

    public List<GenTable> findAll();

    /**
     * 获取物理数据表列表
     * 
     * @param genTable
     * @return
     */
    public List<GenTable> findTableListFormDb(GenTable genTable);

    /**
     * 验证表名是否可用，如果已存在，则返回false
     * 
     * @param genTable
     * @return
     */
    public boolean checkTableName(String tableName);

    /**
     * 获取物理数据表列表
     * 
     * @param genTable
     * @return
     */
    public GenTable getTableFormDb(GenTable genTable);

    public void save(GenTable genTable);

    public void delete(GenTable genTable);

    public GenConfig getGenConfig();
}
