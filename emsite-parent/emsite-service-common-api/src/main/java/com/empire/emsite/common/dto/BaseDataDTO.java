package com.empire.emsite.common.dto;

import java.io.Serializable;
import java.util.Date;

import com.empire.emsite.modules.sys.entity.User;

/**
 * 类BaseDataDTO.java的实现描述：继承DataEntity的实体对应的通用的DTO
 * 
 * @author arron 2018年2月24日 下午4:44:00
 */
public class BaseDataDTO implements Serializable {
    /**
     * 
     */
    private static final long  serialVersionUID = 1L;
    /**
     * 实体编号（唯一标识）
     */
    private String             id;
    /**
     * 当前用户
     */
    private User               currentUser;
    private String             remarks;               // 备注
    private User               createBy;              // 创建者
    private Date               createDate;            // 创建日期
    private User               updateBy;              // 更新者
    private Date               updateDate;            // 更新日期
    private String             delFlag;               // 删除标记（0：正常；1：删除；2：审核）
    /**
     * 删除标记（0：正常；1：删除；2：审核；）
     */
    public static final String DEL_FLAG_NORMAL  = "0";
    public static final String DEL_FLAG_DELETE  = "1";
    public static final String DEL_FLAG_AUDIT   = "2";

    public BaseDataDTO() {
        this.delFlag = DEL_FLAG_NORMAL;
    }

    public BaseDataDTO(String id) {
        this();
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

}
